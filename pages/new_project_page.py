from selenium.webdriver.common.by import By


class NewProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        enter = self.browser.find_element(By.CSS_SELECTOR, '.icon_tools')
        enter.click()

    def add_new_project(self):
        add_project_button = self.browser.find_element(By.CSS_SELECTOR,
                                                       '[href="http://demo.testarena.pl/administration/add_project"]')
        add_project_button.click()

    def fill_new_project_info_and_save(self, name, prefix, description):
        pr_name = self.browser.find_element(By.CSS_SELECTOR, '#name')
        pr_name.send_keys(name)

        pr_prefix = self.browser.find_element(By.CSS_SELECTOR, '#prefix')
        pr_prefix.send_keys(prefix)

        pr_description = self.browser.find_element(By.CSS_SELECTOR, '#description')
        pr_description.send_keys(description)

        submit_button = self.browser.find_element(By.CSS_SELECTOR, '#save')
        submit_button.click()

    def find_added_project(self, ProjectName):
        search_bar = self.browser.find_element(By.CSS_SELECTOR, '#search')
        search_bar.send_keys(ProjectName)
        search_button = self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton')
        search_button.click()
