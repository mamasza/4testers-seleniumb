from selenium.webdriver.common.by import By


class LoginPage:
    URL = 'http://demo.testarena.pl/zaloguj'

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL)

    def login(self, login, password):
        self.browser.find_element(By.CSS_SELECTOR, "#email").send_keys(login)
        self.browser.find_element(By.CSS_SELECTOR, "#password").send_keys(password)
        self.browser.find_element(By.CSS_SELECTOR, "#login").click()
