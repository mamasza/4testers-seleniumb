from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class PanelPage:
    def __init__(self, browser):
        self.browser = browser

    def wait_for_load(self):
        wait = WebDriverWait(sel.browser, 10)
        selector = (By.CSS_SELECTOR, '.j_msgResponse')
        return wait.until(expected_conditions.element_to_be_clickable(selector))


class CockpitPage:
    URL = 'http://demo.testarena.pl/zaloguj'
    suffix = '/moje_wiadomosci'

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL + self.suffix)
        envelope = browser.find_element(By.CSS_SELECTOR, '.top_messages')
        envelope.click()


class NavBar:
    def __init__(self, browser):
        self.browser = browser

    def go_to_main_menu(self):
        header_logo = self.browser.find_element(By.CSS_SELECTOR, '#header_logo')
        header_logo.click()

    def go_to_projects(self):
        projects_icon = self.browser.find_element(By.CSS_SELECTOR,
                                                  '[href="http://demo.testarena.pl/UJMS01/project_view"]')
        projects_icon.click()
