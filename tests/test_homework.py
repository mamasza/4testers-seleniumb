import pytest
import random
import string

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from faker import Faker
from pages.new_project_page import NewProjectPage
from pages.login_page import LoginPage
from pages.cockpit_page import NavBar


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    new_project_page = NewProjectPage(browser)
    nav_bar = NavBar(browser)
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    new_project_page.load()
    new_project_page.add_new_project()
    new_project_page.fill_new_project_info_and_save(new_project_name, Faker().last_name(), str(Faker().random))
    nav_bar.go_to_main_menu()
    new_project_page.load()
    new_project_page.find_added_project(new_project_name)
    yield browser
    browser.quit()


new_project_name = Faker().last_name()


def test_find_new_project_by_name(browser):

    assert browser.find_element(By.LINK_TEXT,new_project_name).is_displayed()


