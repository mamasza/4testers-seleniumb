
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
import time
def test_example():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.bing.com/")
    driver.fullscreen_window()
    time.sleep(2)
    driver.quit()
